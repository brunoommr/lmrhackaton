# BACK END 
## DESARROLLADOR FRONT END
Es aquel que se encargar de realizar la parte estetica visual de la pagina  web.
### Tecnologias de desarrollo que un front-end debe conocer
Debe conocer HTML par la estructura de la pagina, CSS se encarga de los estilos visuales (Apariencia que tendra la pagina), JS se encarga de la interactividad de la pagina.
En el desarrollo tambien existe algunas herramientas que facilitan el desarrollo como el JADE(Motor de planitllas), SASS(reprocesador CSS), TypeScript(Lenguaje de Comunicacionq que pretende facilitar la estructura)
### Actividades de un front-end
-Desarrollo del prototipo de la web usando herramientas como sketch, adobe XD, figma   
-Creacion de la estructura base del proyeco apoyandose en ciertos framework 
-Progamar la web
-Implementar la pagina bajo sus productos
### Cual es el rol del Front end
El rol del front end es implementar una app que sea visualmente atractiva (UI) y que brinden una experiencia    agrabable al usuario (UX)
que cumpla los estandares de calidad

### Que es lo que mas destaca las siguienes paginas web
WordPress 
Es un CMS la cual tiene una interfaz ligera, facil de usar y cuenta con buena interfaz sin tanto contenido.

GitLab
Notamos que esta usando una libreria ligera  Vue, su interfaz es limpia.

### Tendencias visaules 
Fuentes: tamaño adecuado de las fuentes y diferentes tipo de apariencia

### Tendencias Front-end
PWA: Progresive Web Application nos permite usar una pagina web como si fuera una aplicacion nativa 
haciendo uso de los service worker
Web Assembly: Nos permite potenciar nuestra aplicacion Front end, dandole el poder de forma casi nativa.
Server Less: Apoya a los programadores front end al evitar que sea dependiente de un backend en si, haciendo uso de funciones en la nube para realizar las labores backend

## Back End

### Tecnologias de desarrollo de un backend 
-Manejo basico de la linea de comandos
-Uso de un sistema de control de versiones (Git)
-Conocer un lenguaje de programacion como Java, C#, Pyhton
-Manejar frameworks para el facilitar el desarrollo
-Manejo de base de datos
-Uso de contenedores

### Lenguajes de programacion y Framework
LP:
Java, C#, Python, Ruby
Frameworks:
Spring, Django, Symfony, Laravel.

### Base de Datos
Relacionales: Oracle SQL server, Postgress, MariaDB
No relacionales: MongoDB

### Servidores Web y protocolos
Nginx
Apache

### Actividades de un Backend
Analizar, coordinar 

# Conceptos que debe tener un desarrollador backend
Performacne infraestrucutar, Seguridad y Automatizacion 