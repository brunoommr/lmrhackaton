# Hackaton Semana 1
## **LOGRO**: gestiona las versiones del código con GIT según el lenguaje utilizado
**“Sé colaborador en Gitlab”**
Llegó el momento de demostrar lo aprendido durante esta semana, para esto deberás cumplir un reto: 

![](tereto.jpg)

**Crea un proyecto simple donde se tengan carpetas (como lo que vimos en la sesión 3) y súbelo a Gitlab**

### Insumos para resolver el Reto

 - Materiales de clase de la semana 1 
 - Git book https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Acerca-delControl-de-Versiones 
 - Gitlab merge request https://docs.gitlab.com/ee/user/project/merge_requests/

### Pasos a seguir para resolver el Reto
1. Realizar un **FORK** al repositorio https://gitlab.com/rpinedaec/pachaqtecoctubre2020.git
2. Clonar desde nuestro propio repositorio el proyecto
3.  Sube tus cambios a tu repositorio local.  
4. Asocia tu repositorio local al compartido (remoto) y sube tus cambios. 
5. Envía el **merge request** para integrar los cambios
### Solución del Reto
 - El proyecto debe tener una carpeta con el README.md en el que estará nombre del colaborador y el nombre de su primer proyecto.
 - Además vamos a redactar los conceptos de la primera clase (Sesión 1 Semana 1)
 - El repositorio deberá contar con commits y su push en la rama principal (master)

### Reto cumplido
Para que el reto esté cumplido al 100% deberás tener cubierto los requisitos básicos expuestos: 
- Que se tenga en el gitlab 1 carpeta con el nombre (inicail del nombre y el apellido del estudiante) Dentro de ella, se agregarán archivos referentes a la redacción de la primera clase (Sesión 1 Semana 1) 
- Revisar que el proyecto (repositorio) tenga un README.md con el nombre del colaborador y del proyecto. 
- Revisar que contengan commits y el push en la rama principal.